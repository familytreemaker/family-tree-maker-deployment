# How to run

```
./kubectlapplycommands.sh
```

# Demo

Visit http://familytreemaker.xyz

User ID: `demo@familytreemaker.xyz`
Password: `familytreemaker`

# Install nginx ingress controller

```
helm install quickstart ingress-nginx/ingress-nginx
```

# Cert

See

- https://cert-manager.io/docs/tutorials/acme/ingress/#step-5-deploy-cert-manager
- https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nginx-ingress-with-cert-manager-on-digitalocean-kubernetes

Installing cert-manager with helm:

https://cert-manager.io/docs/installation/helm/

## Command to install cert-manager

```
helm install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --create-namespace \
  --version v1.6.1 \
  --set installCRDs=true
```

# How to setup cert on DigitalOcean

1. Create a kubernetes cluster. Do NOT select nginx controller installation
1. Install the nginx ingress controller with the helm command above
1. Install the cert manager with the helm command above
1. Create the required resources with `./kubectlapplycommands.sh`
1. Get the load balancer's IP address and update your DNS to point to the load balancer
1. Wait for 15 mins to get the cert issued

## Local Development

I tested this on my ZorinOS desktop with `minikube` as the cluster software.

### Steps

#### Building the images

Build your images as you would normally, then push to a Docker repo.

**Build and push - Webapp**

See the webapp's README

**Build and push - Backend**

See the backend's README

#### Deploying to a local cluster

1. Create a cluster. Follow steps in https://minikube.sigs.k8s.io/docs/start/
   1. For me, I had to use the `--driver=kvm2` option
1. Do `minikube dashboard`
1. Do `minikube addons enable ingress`
1. Do `minikube tunnel`
1. Do `./kubectlapplycommands.local.sh`
1. Get minikube ip: `minikube ip`
1. Add this line to your /etc/hosts file: `<the minikube ip> familytreemaker.xyz`
1. Open a browser and go to `http://familytreemaker.xyz`

#### Using local Docker images
